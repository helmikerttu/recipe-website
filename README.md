# Recipe website

This is my first website ever made with pure HTML and CSS that scales with screen width. Here are two images of the front page, one on narrow and other on wide screen:

![Narrow screen](https://i.imgur.com/8DrIbQE.png)

![Wide screen](https://i.imgur.com/ziJiPTc.png)